# Plug

> minimalistic plugin framework

Plug is a minimalistic framework that turns Python classes into command lines programs.

It has just one class (Plug) that search given folder for .py files, load any module, class, class parameters, function, function parameters and turns them to command line arguments using argparse.

\*Inteded for personal use and tested only under linux 


# Install


Using pip 

```bash
pip install git+https://gitlab.com/vagmakr/plug.git --user
```

Or directly from Source 

```bash
git clone https://gitlab.com/vagmakr/plug.git
cd plug
python setup.py install
```


# Usage

Choose one or more folders to store your plugins.
We will use /tmp/plugins for this example.

```bash
export PLUG_PATH=/tmp/plugins
```


Write a simple plugin (/tmp/plugins/example.py)

```python
class Castle(object):
  def __init__(self, height:int=1000):
    self.height = height

  def info():
    print(self.height)

  def destroy():
    self.height = 0
    self.dump()
```

Then try it in cli

```bash
> plug --help
> plug example.Castle --help
> plug example.Castle info
1000
> plug example.Castle --height 100 info
100
> plug example.Castle info --height 100 
usage: plug [-h] {example.Castle} ...
plug: error: unrecognized arguments: --height 100
> plug example.Castle destroy
0 
``` 


# Development

Import and run plug in your script

```python
from plug import Plug

p = Plug('./plugins')
p.run()
```


# TODO

- Use class transfomration to Api (like Argparse to cli)
- Use a SimpleHttpServer in run function
- Reload plugins in the background
