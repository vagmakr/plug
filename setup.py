from setuptools import setup, find_packages

def readme():
    with open('README.md') as f:
        return f.read()

setup(
    name='plug',
    version='0.1.1',
    description='Minimalistic Pluggin System',
    url='https://gitlab.com/vagmakr/plug.git',
    author='vagmakr',
    author_email='vagmakr@gmail.com',
    license='GNU',
    packages=find_packages(),
    install_requires=[],
    zip_safe=False,
    package_data={},
    include_package_data=True,
    # Setup script
    scripts=[
        "bin/plug"
    ],
)
