# main.py
import os
import sys
import inspect
import importlib.util
import argparse
import types
import json


TYPES = {
  'int' : int,
  'str' : str,
  'dict': dict,
  'list': list,
  'open': open,
}



class Plug(object):
    """Plug 
    
    """
    
    def __init__(self, *paths):
        self.paths = paths

    def _get_class_from_name(self, fullname):
        for _module, _class, _class_info in self.load_classes():
            if _class_info["fullname"] == fullname:
                return _class
        return None

    def _parameter_type(self, parameter):
        _type_name = parameter.__str__().split('=')[0].strip()
        if len(_type_name.split(':')) > 1:
            _type_name = _type_name.split(':')[1].strip()
            try:
                # For some reason cam't load builtin types
                # using getattr(__builtins__, 'int'). It works
                # in Interactive Python but not here.
                #
                # Temporary solution is to load types from TYPES dict
                #
                return getattr(__builtins__, _type_name)
            except AttributeError:
                if TYPES.get(_type_name):
                    return TYPES[_type_name]
        return str

    def _add_argument_arguments(self, parameter):
        _args   = ['--{}'.format(parameter.name)]
        _kwargs = {}
        _help   = parameter.name

        # if parameter has an explicit type, add it
        #   >>> def func(a:open, b:int=0):pass
        # will translate to:
        #   >>> parser.add_argument('a', type=open) # for opening file
        #   >>> parser.add_argument('b', type=int)  # for intreger
        _type = self._parameter_type(parameter)
        _help += ' type:{}'.format(_type.__name__)
        _kwargs["type"] = _type

        
        # if parameter has default variable add it
        # >>> parser.add_argument('foo', default="whatever")
        if not isinstance(parameter.default, inspect._empty): # ! gives error for str
        # if type(parameter.default()) in dir(__builtins__):
            _kwargs["default"] = parameter.default
            _help += ' (default:{})'.format(parameter.default)

        # help of parameter
        _kwargs["help"] = _help
        
        return _args, _kwargs
    
    def _dump(self):
        for _module, _class, _class_info in self.load_classes():
            for _func, _func_info in self.load_functions(_class):
                print(_module, _class, _func)
        
    def _returns(self, f):
        if  "return" in inspect.getsource(f):
            return True
        return False

    def _yields(self, f):
        if  "yield" in inspect.getsource(f):
            return True
        return False

    def object_info(self, obj, notallowed=["self", "args", "kwargs"]):
        info = dict()
        info["doc"]        = obj.__doc__
        info["name"]       = obj.__name__ 
        info["module"]     = obj.__module__
        info["fullname"]   = ".".join([obj.__module__, obj.__name__])
        info["help"]       = ".".join([obj.__module__, obj.__name__]) + " help"
        sig  = inspect.signature(obj)
        # info["returns"]    = not isinstance(sig.return_annotation(), inspect._empty)
        info["parameters"] = []
        for parameter in sig.parameters.values():
            if parameter.name not in notallowed:
                info["parameters"].append(parameter)
        # info["function"] = isinstance(obj, types.FunctionType)
        # info["class"]    = isinstance(obj, types.FunctionType)
        return info

    def find_modules(self, *paths):
        for path in paths:
            for fl in os.listdir(path):
                if fl.endswith('.py'):
                    yield '{}/{}'.format(path,fl)

    def load_module(self, path):
        module_name = inspect.getmodulename(path)
        spec        = importlib.util.spec_from_file_location(module_name, path)
        module      = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        sys.modules[spec.name] = module
        return module    

    def load_classes(self, paths=[]):
        if not paths:
            paths = self.paths

        try:
 
            for path in self.find_modules(*paths):
                
                module = self.load_module(path)

                _class_names = [ c for c in dir(module) if not c.startswith('__') ]
                if hasattr(module, "__all__"):
                    _class_names = [ c for c in getattr(module, "__all__") if not c.startswith('__') ]
                
                for _class_name in _class_names:
                    _class = getattr(module, _class_name)
                    yield module, _class, self.object_info(_class) 

        except Exception as e:
            # for _m, _c, _i in self.load_classes(paths):
            #     yield _m, _c, _i
            raise e
    
    def load_functions(self, _class):

        # Load function names from class
        _func_names = [ _f for _f in dir(_class) if ( not _f.startswith('_') ) and hasattr(getattr(_class, _f), "__call__") ]

        # if __all__ is explicetely specified, use it
        if hasattr(_class, "__all__"):
            _func_names = [ f for f in getattr(_class, "__all__") if not f.startswith('__') and hasattr(getattr(_class, f), "__call__") ]

        # iterate through functions
        for _func_name in _func_names:
            _func = getattr(_class, _func_name)
            yield _func, self.object_info(_func)

    def cli(self, parser=None, **kwargs):
        
        # If parser is not provided create one
        if ( not parser ) or ( not isinstance(parser, argparse.ArgumentParser) ) :
            parser = argparse.ArgumentParser(**kwargs)
        
        # Add subparser classes
        classparsers = parser.add_subparsers(dest='plugin', help="plugin help")

        # Iterate through classes
        for _module, _class, _class_info in self.load_classes():
            
            # Add parser for each class
            p = classparsers.add_parser(_class_info["fullname"], help=_class_info["help"], description=_class_info["doc"])
            
            # iterete through class initialization parameters
            for parameter in _class_info["parameters"]:

                # turn parameter into argument
                _args, _kwargs = self._add_argument_arguments(parameter)
                p.add_argument(*_args, **_kwargs)
            
            # # Inject _argparsin to the class
            # setattr(_class, "_argparsin_class", _argparsin)

            # Set class subparser to bind to injected function
            p.set_defaults(func=_class)

            # Add subparser for functions
            funcparsers = p.add_subparsers(dest='action', help='action help')

            # iterete through each function
            for _func, _func_info in self.load_functions(_class):
                
                # Add parser for each function 
                f = funcparsers.add_parser(_func_info["name"], help=_func_info["help"], description=_func_info["doc"])
                
                # iterete through each function parameters
                for parameter in _func_info["parameters"]:
                
                    # turn parameter into argument
                    _args, _kwargs = self._add_argument_arguments(parameter)
                    f.add_argument(*_args, **_kwargs)
                
                # # Inject _argparsin_func to the class
                # setattr(_class, "_argparsin_func", _argparsin_func)

                # Set function subparser to bind to injected function
                f.set_defaults(func=_class)

        # **TODO**:
        # => Instead of return use: 
        #   >> args = parser.parse_args()
        #   >> args.func(args)
        #   As is described in following article:
        #   https://medium.com/@george.shuklin/argparse-how-to-get-command-name-for-subparsers-d836483e9511
        return self._argparsin(parser)

    def _load_args_from_argparse(self, args, parameters):
        _args = []
        for parameter in parameters:
            if hasattr(args, parameter.name):
                _args.append(
                    # self._parameter_type(parameter)(getattr(args, parameter.name))
                    getattr(args, parameter.name)
                )
                delattr(args, parameter.name)
        return _args


    def _argparsin(self, parser):        
        """
        Dirty dirty dirty
        """
        args = parser.parse_args()

        if not args.plugin:
            print("`plugin` argument is missing ! use --help")
            sys.exit(1)

        _class = args.func
        _args  = self._load_args_from_argparse(args, self.object_info(_class)["parameters"])
        C      = _class(*_args)

        # load function stuff
        if not args.action:
            print("`action` argument is missing ! use {} --help".format(args.plugin))
            sys.exit(1)

        _func      = getattr(C, args.action)
        _func_info = self.object_info(getattr(_class, args.action))
        _args      = self._load_args_from_argparse(args, _func_info["parameters"])

        # If function returns or yields something
        # print it out, otherwise just execute the function
        if self._returns(_func):
            self.print(_func(*_args))

        elif self._yields(_func):
            for i in _func(*_args):
                self.print(i)

        else:
            _func(*_args)
    
    def print(self, *args):
        for arg in args:
            if type(arg) in [int, float, str]:
                sys.stdout.write(arg)
            elif type(arg) in [ dict, list]:
                sys.stdout.write(json.dumps(arg, indent=2))
            elif type(arg) in [ bool ]:
                sys.stdout.write(str(arg))
            elif type(arg) in [ bytes ]:
                sys.stdout.write(arg.decode())
            sys.stdout.write("\n")


